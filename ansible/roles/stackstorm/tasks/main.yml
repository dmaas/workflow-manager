# XXX This sucks; we need emulab.common.common to split out the per-expt
# ssh stuff so we can leverage it.
- name: Get public half of Emulab per-expt key
  when: geni_pubkey is not defined
  shell: "ssh-keygen -f /users/{{ emulab_swapper }}/.ssh/{{ emulab_ssh_keypair_name }} -y"
  register: ret

- ansible.builtin.set_fact:
    geni_pubkey: "{{ ret.stdout_lines[0] }}"
  when: geni_pubkey is not defined

- name: Create Stackstorm setup dir
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.file:
    path: "{{ stackstorm_setup_dir }}"
    state: directory

- name: Clone st2-docker repo
  become_user: "{{ emulab_swapper }}"
  become: true
  shell: "git clone --depth 1 https://github.com/stackstorm/st2-docker.git {{ stackstorm_setup_dir }}/st2-docker/"
  args:
    creates: "{{ stackstorm_setup_dir }}/st2-docker/"
  register: ret
  until: ret.rc == 0
  retries: 30
  delay: 5

#- name: Override upstream docker-compose.yml
#  ansible.builtin.template:
#    src: docker-compose.yml
#    dest: "{{ kiwitcms_setup_dir }}/kiwi-tcms/docker-compose.yml"

- name: Install python passlib for htpasswd
  ansible.builtin.package:
    name: python{{ ansible_python_version | first | replace('2','') }}-passlib

- name: Update stackstorm st2admin passwords
  become_user: "{{ emulab_swapper }}"
  become: true
  block:
    - ansible.builtin.htpasswd:
        name: st2admin
        password: "{{ stackstorm_admin_password }}"
        path: "{{ stackstorm_setup_dir }}/st2-docker/files/htpasswd"
        state: present
    - community.general.ini_file:
        path: "{{ stackstorm_setup_dir }}/st2-docker/files/st2-cli.conf"
        section: credentials
        option: password
        value: "{{ stackstorm_admin_password }}"

- name: Enable code sharing between stackstorm actions and sensors
  community.general.ini_file:
    path: "{{ stackstorm_setup_dir }}/st2-docker/files/st2.user.conf"
    section: packs
    option: enable_common_libs
    value: "True"

- name: Configure public URL
  community.general.ini_file:
    path: "{{ stackstorm_setup_dir }}/st2-docker/files/st2.user.conf"
    section: webui
    option: webui_base_url
    value: "https://{{ emulab_fqdn }}:{{ stackstorm_public_port }}"

- name: Check if we generated an API key for chatops already
  when: "stackstorm_slack_token != ''"
  shell: "sed -nre 's/^ST2_API_KEY=(.+)$/\\1/p' {{ stackstorm_setup_dir }}/st2-docker/.env | tail -1"
  register: ret_apikey
  failed_when: False
  changed_when: False

- ansible.builtin.set_fact:
    stackstorm_api_key: "{{ ret_apikey.stdout }}"
  when: "stackstorm_slack_token != '' and ret.rc != 0"
  failed_when: False
  changed_when: False

- name: Define stackstorm env overrides
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.template:
    src: stackstorm.env
    dest: "{{ stackstorm_setup_dir }}/st2-docker/.env"
    mode: '0755'

- name: Create stackstorm packs dir
  become_user: "{{ emulab_swapper }}"
  become: true
  ansible.builtin.file:
    path: "{{ stackstorm_setup_dir }}/packs"
    mode: '0755'
    state: directory

- name: Add stackstorm-emulab pack
  become_user: "{{ emulab_swapper }}"
  become: true
  shell: "git clone https://gitlab.flux.utah.edu/emulab/stackstorm-emulab.git {{ stackstorm_setup_dir }}/packs/emulab"
  args:
    creates: "{{ stackstorm_setup_dir }}/packs/emulab"

- name: Add python docker packages
  ansible.builtin.package:
    name:
      - python{{ ansible_python_version | first | replace('2','') }}-docker
      - python{{ ansible_python_version | first | replace('2','') }}-yaml
      - docker-compose
    state: present

- name: Start up Stackstorm via docker-compose
  community.docker.docker_compose:
    project_src: "{{ stackstorm_setup_dir }}/st2-docker"
    state: present
  register: ret
  until: ret is success
  retries: 30
  delay: 5

- name: Handle stackstorm chatops API key
  block:
    - name: Check if we have an API key for chatops
      when: "stackstorm_slack_token != ''"
      shell: 'grep -q "^ST2_API_KEY" {{ stackstorm_setup_dir }}/st2-docker/.env'
      register: ret
      failed_when: False
      changed_when: False

    - name: Get API key for chatops
      when: "stackstorm_slack_token != '' and ret.rc != 0"
      shell: 'docker-compose exec -T st2client st2 apikey create -k -m "{\"used_by\": \"chatops\"}"'
      args:
        chdir: "{{ stackstorm_setup_dir }}/st2-docker"
      register: ret_apikey

    - ansible.builtin.set_fact:
        stackstorm_api_key: "{{ ret_apikey.stdout }}"
      when: "stackstorm_slack_token != '' and ret.rc != 0"

    - name: Reapply stackstorm env overrides
      when: "stackstorm_slack_token != '' and ret.rc != 0"
      ansible.builtin.template:
        src: stackstorm.env
        dest: "{{ stackstorm_setup_dir }}/st2-docker/.env"
        owner: "{{ emulab_swapper }}"
        group: "{{ emulab_swapper_group }}"
        mode: '0755'

    - name: Restart stackstorm
      when: "stackstorm_slack_token != '' and ret.rc != 0"
      community.docker.docker_compose:
        project_src: "{{ stackstorm_setup_dir }}/st2-docker"
        state: present
        restarted: true

- name: Wait until Stackstorm is up
  shell: docker-compose exec -T st2client st2 whoami
  args:
    chdir: "{{ stackstorm_setup_dir }}/st2-docker"
  register: ret
  until: ret.rc == 0
  retries: 120
  delay: 5

- name: Register all resources
  block:
    # Use GNU timeout since task.timeout didn't arrive until ansible 2.10
    - shell: timeout 120 docker-compose exec -T st2api st2ctl reload --register-all
      args:
        chdir: "{{ stackstorm_setup_dir }}/st2-docker"
      register: ret
      until: ret.rc == 0
      retries: 30
      delay: 5
    - shell: timeout 120 docker-compose exec -T st2client st2ctl reload --register-all
      args:
        chdir: "{{ stackstorm_setup_dir }}/st2-docker"
      register: ret
      until: ret.rc == 0
      retries: 30
      delay: 5

- name: Register Emulab pack
  shell: docker-compose exec -T st2client st2 pack register emulab
  args:
    chdir: "{{ stackstorm_setup_dir }}/st2-docker"

- name: Install Emulab pack configuration
  block:
    - ansible.builtin.template:
        src: configs-emulab.yaml
        dest: "{{ stackstorm_setup_dir }}/configs-emulab.yaml"
        owner: "{{ emulab_swapper }}"
        group: "{{ emulab_swapper_group }}"
        mode: '0644'
    - shell: "docker cp {{ stackstorm_setup_dir }}/configs-emulab.yaml st2-docker_st2client_1:/opt/stackstorm/configs/emulab.yaml"
    - name: Push Emulab pack configuration
      shell: docker-compose exec -T st2client st2ctl reload --register-configs --verbose
      args:
        chdir: "{{ stackstorm_setup_dir }}/st2-docker"

- name: Install Emulab pack virtualenv
  shell: "docker-compose exec -T st2client st2 run packs.setup_virtualenv packs=emulab"
  args:
      chdir: "{{ stackstorm_setup_dir }}/st2-docker"

- name: Install POWDER O-RAN pack
  include_tasks: oran_pack.yml

- name: Install POWDER KiwiTCMS pack
  include_tasks: kiwitcms_pack.yml

- name: Install POWDER O-RAN profile pack
  include_tasks: oran_profile_pack.yml

- name: Install Keysight RICtest pack
  include_tasks: keysight_rictest_pack.yml

- name: Install UE Matrix pack
  include_tasks: ue_matrix_pack.yml

- name: Create nginx reverse proxy for stackstorm
  when: stackstorm_reverse_proxy_key_path and stackstorm_reverse_proxy_cert_path
  block:
    - name: Install nginx
      ansible.builtin.package:
        name: nginx
        state: present

    - name: Create reverse proxy site
      ansible.builtin.template:
        src: nginx-reverse-proxy.conf
        dest: /etc/nginx/sites-available/stackstorm-reverse-proxy.conf
        mode: '0755'
      notify:
        - Reload nginx

    - name: Enable reverse proxy site
      ansible.builtin.file:
        src: /etc/nginx/sites-available/stackstorm-reverse-proxy.conf
        dest: /etc/nginx/sites-enabled/stackstorm-reverse-proxy.conf
        state: link
      notify:
        - Reload nginx
