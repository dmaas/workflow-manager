#
# Stub code for booting up first time, intended to be replaced later.
#
from dagster import fs_io_manager, job, graph, op, repository, schedule

@op
def hello():
    return 1


@op
def goodbye(foo):
    if foo != 1:
        raise Exception("Bad io manager")
    return foo * 2


@graph
def my_graph():
    goodbye(hello())

@job(
    resource_defs={
        "io_manager": fs_io_manager.configured({"base_dir":
                                                "/tmp/io_manager_storage"})
    },
)
def myjob():
    my_graph()
    pass
