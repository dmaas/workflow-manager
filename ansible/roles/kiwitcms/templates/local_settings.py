CSRF_TRUSTED_ORIGINS = [ "http://{{ emulab_fqdn }}", "http://{{ emulab_fqdn }}:8444",  "https://{{ emulab_fqdn }}", "https://{{ emulab_fqdn }}:8444", "http://{{ emulab_fqdn | lower }}", "http://{{ emulab_fqdn | lower }}:8444",  "https://{{ emulab_fqdn | lower }}", "https://{{ emulab_fqdn | lower }}:8444" ]
USE_TZ = True
KIWI_TIME_ZONE = "America/Denver"
