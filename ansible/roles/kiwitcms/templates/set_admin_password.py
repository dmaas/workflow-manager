from django.contrib.auth.models import User

adminuser = User.objects.get(username='admin')
adminuser.set_password('{{ kiwitcms_admin_password }}')
adminuser.save()
