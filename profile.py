import hashlib
import random
from lxml import etree as ET

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the InstaGENI library.
import geni.rspec.igext as ig
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab
# Import Emulab Ansible-specific extensions.
import geni.rspec.emulab.ansible
from geni.rspec.emulab.ansible import (Role, RoleBinding, Override, Playbook)


class EmulabEncrypt(pg.Resource):
    def __init__(self,name):
        super(EmulabEncrypt,self).__init__()
        self.name = name

    def _write(self, root):
        ns = "{%s}" % (pg.Namespaces.EMULAB,)
        el = ET.SubElement(root,"%spassword" % (ns,),attrib={'name':self.name})

HEAD_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '/local/repository/emulab-ansible-bootstrap/head.sh >/local/logs/setup.log 2>&1'"
CLIENT_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '/local/repository/emulab-ansible-bootstrap/client.sh >/local/logs/setup.log 2>&1'"

#
# For now, disable the testbed's root ssh key service until we can remove ours.
# It seems to race (rarely) with our startup scripts.
#
#disableTestbedRootKeys = True

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Add some custom roles to the rspec; we will bind them to nodes later.
request.addRole(
    Role("stackstorm",path="ansible",playbooks=[
        Playbook("stackstorm",path="stackstorm.yml",become="root")]))
request.addRole(
    Role("kiwitcms",path="ansible",playbooks=[
        Playbook("kiwitcms",path="kiwitcms.yml",become="root")]))
request.addRole(
    Role("dagster",path="ansible",playbooks=[
        Playbook("dagster",path="dagster.yml",become="root")]))

nodetypelist = ["d430","d740","d710"]

agglist = [
    ("urn:publicid:IDN+emulab.net+authority+cm","emulab.net"),
    ("urn:publicid:IDN+utah.cloudlab.us+authority+cm","utah.cloudlab.us"),
    ("urn:publicid:IDN+clemson.cloudlab.us+authority+cm","clemson.cloudlab.us"),
    ("urn:publicid:IDN+wisc.cloudlab.us+authority+cm","wisc.cloudlab.us"),
    ("urn:publicid:IDN+apt.emulab.net+authority+cm","apt.emulab.net"),
    ("","Any")
]

pc.defineParameter(
    "installStackStorm","Install StackStorm",
    portal.ParameterType.BOOLEAN,True,
    longDescription="Install StackStorm workflow management software.")
pc.defineParameter(
    "installKiwiTCMS","Install Kiwi TCMS",
    portal.ParameterType.BOOLEAN,True,
    longDescription="Install Kiwi Test Case Management System.")
pc.defineParameter(
    "installDagster","Install Dagster",
    portal.ParameterType.BOOLEAN,False,
    longDescription="Install Dagster orchestrator")
#pc.defineParameter(
#    "installRobot","Install Robot Framework",
#    portal.ParameterType.BOOLEAN,True,
#    longDescription="Install Robot Framework test automation software.")
#pc.defineParameter(
#    "installInfluxDB","Install InfluxDB",
#    portal.ParameterType.BOOLEAN,True,
#    longDescription="Install InfluxDB (metrics capture).")
#pc.defineParameter(
#    "installGrafana","Install Grafana",
#    portal.ParameterType.BOOLEAN,True,
#    longDescription="Install Grafana (metrics dashboard UI).")
pc.defineParameter(
    "hwtype", "Workflow Manager Node Type",
    portal.ParameterType.STRING, nodetypelist[0], nodetypelist,
    advanced=True,
    longDescription="Hardware type for compute node that will act as the workflow manager.")
pc.defineParameter(
    "routableIP","Routable IP",
    portal.ParameterType.BOOLEAN,True,
    longDescription="Add a routable IP to the VM.")
pc.defineParameter(
    "sslCertType","SSL Certificate Type",
    portal.ParameterType.STRING,"letsencrypt",
    [("none","None"),("selfsigned","Self-Signed"),("letsencrypt","Let's Encrypt")],
    advanced=True,
    longDescription="Choose an SSL Certificate strategy.  By default, we generate self-signed certificates, and only use them for a reverse web proxy to allow secure remote access to the Kubernetes Dashboard.  However, you may choose `None` if you prefer to arrange remote access differently (e.g. ssh port forwarding).  You may also choose to use Let's Encrypt certificates whose trust root is accepted by all modern browsers.")
pc.defineParameter(
    "image","Node Image",portal.ParameterType.STRING,
    'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD',
    longDescription="The image your node will run.",
    advanced=True)
pc.defineParameter(
    "aggregate","Specific Aggregate",portal.ParameterType.STRING,
    "urn:publicid:IDN+emulab.net+authority+cm",agglist,
    advanced=True)
pc.defineParameter(
    "emulabXmlrpcServer","Emulab XMLRPC Server Hostname",portal.ParameterType.STRING,"",
    longDescription="Override the default XMLRPC server.",
    advanced=True)
pc.defineParameter(
    "emulabXmlrpcPort","Emulab XMLRPC Server Port",portal.ParameterType.STRING,"",
    longDescription="Override the default XMLRPC server port (e.g., 45454 if instructed to use johnsond's development environment).",
    advanced=True)
pc.defineParameter(
    "emulabXmlrpcPath","Emulab XMLRPC Server Path",portal.ParameterType.STRING,"",
    longDescription="Override the default XMLRPC server port (e.g., /usr/testbed/devel/johnsond if instructed to use johnsond's development environment).",
    advanced=True)
pc.defineStructParameter(
    "sharedVlans","Add Shared VLAN",[],
    multiValue=True,itemDefaultValue={},min=0,max=None,
    members=[
        portal.Parameter(
            "createConnectableSharedVlan","Create Connectable Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Create a placeholder, connectable shared VLAN stub and 'attach' the first node to it.  You can use this during the experiment to connect this experiment interface to another experiment's shared VLAN."),
        portal.Parameter(
            "createSharedVlan","Create Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Create a new shared VLAN with the name above, and connect the first node to it."),
        portal.Parameter(
            "connectSharedVlan","Connect to Shared VLAN",
            portal.ParameterType.BOOLEAN,False,
            longDescription="Connect an existing shared VLAN with the name below to the first node."),
        portal.Parameter(
            "sharedVlanName","Shared VLAN Name",
            portal.ParameterType.STRING,"",
            longDescription="A shared VLAN name (functions as a private key allowing other experiments to connect to this node/VLAN), used when the 'Create Shared VLAN' or 'Connect to Shared VLAN' options above are selected.  Must be fewer than 32 alphanumeric characters."),
        portal.Parameter(
            "sharedVlanAddress","Shared VLAN IP Address",
            portal.ParameterType.STRING,"10.254.254.1",
            longDescription="Set the IP address for the shared VLAN interface.  Make sure to use an unused address within the subnet of an existing shared vlan!"),
        portal.Parameter(
            "sharedVlanNetmask","Shared VLAN Netmask",
            portal.ParameterType.STRING,"255.255.255.0",
            longDescription="Set the subnet mask for the shared VLAN interface, as a dotted quad.")])

# Map some parameters to ansible overrides.
request.addOverride(
    Override("emulab_ssl_cert_type",source="parameter",source_name="emulab.net.parameter.sslCertType"))
request.addOverride(
    Override("stackstorm_emulab_xmlrpc_server",source="parameter",
             on_empty=False,source_name="emulab.net.parameter.emulabXmlrpcServer"))
request.addOverride(
    Override("stackstorm_emulab_xmlrpc_port",source="parameter",
             on_empty=False,source_name="emulab.net.parameter.emulabXmlrpcPort"))
request.addOverride(
    Override("stackstorm_emulab_xmlrpc_path",source="parameter",
             on_empty=False,source_name="emulab.net.parameter.emulabXmlrpcPath"))

# Map some passwords to ansible overrides.
request.addOverride(
    Override("stackstorm_admin_password",source="password",source_name="perExptPassword"))
request.addOverride(
    Override("kiwitcms_admin_password",source="password",source_name="perExptPassword"))
request.addOverride(
    Override("dagster_admin_password",source="password",source_name="perExptPassword"))

params = pc.bindParameters()

i = 0
for x in params.sharedVlans:
    n = 0
    if x.createConnectableSharedVlan:
        n += 1
    if x.createSharedVlan:
        n += 1
    if x.connectSharedVlan:
        n += 1
    if n > 1:
        err = portal.ParameterError(
            "Must choose only a single shared vlan operation (create, connect, create connectable)",
        [ 'sharedVlans[%d].createConnectableSharedVlan' % (i,),
          'sharedVlans[%d].createSharedVlan' % (i,),
          'sharedVlans[%d].connectSharedVlan' % (i,) ])
        pc.reportError(err)
    if n == 0:
        err = portal.ParameterError(
            "Must choose one of the shared vlan operations: create, connect, create connectable",
        [ 'sharedVlans[%d].createConnectableSharedVlan' % (i,),
          'sharedVlans[%d].createSharedVlan' % (i,),
          'sharedVlans[%d].connectSharedVlan' % (i,) ])
        pc.reportError(err)
    i += 1

tourDescription = \
  "This profile creates a shared VM that can execute complex multi-experiment workflows in this testbed, using Stackstorm (workflow engine, with Emulab API plugin), Ansible (configuration management automation, with Emulab integration), Robot Framework (test automation), and KiwiTCMS (test case management)."

tourInstructions = \
 """
Once your experiment completes setup, you will be able to access
 """
if params.installStackStorm:
    tourInstructions += \
    """
  - [The StackStorm web interface](https://{host-node-0}:8443/) (user `st2admin`, password `{password-perExptPassword}`)"""

if params.installKiwiTCMS:
    tourInstructions += \
    """
  - [The KiwiTCMS web interface](https://{host-node-0}:8444/) (user `admin`, password `{password-perExptPassword}`)"""

if params.installDagster:
    tourInstructions += \
    """
  - [The Dagster web interface](https://{host-node-0}:8445/) (user `admin`, password `{password-perExptPassword}`)"""

tour = ig.Tour()
tour.Description(ig.Tour.TEXT,tourDescription)
tour.Instructions(ig.Tour.MARKDOWN,tourInstructions)
request.addTour(tour)

sharedvlans = []
node = pg.RawPC("node-0")
node.hardware_type = params.hwtype
if params.aggregate:
    node.component_manager_id = params.aggregate
if params.image:
    node.disk_image = params.image
#if disableTestbedRootKeys:
#    node.installRootKeys(False, False)
if params.installStackStorm:
    node.bindRole(RoleBinding("stackstorm"))
if params.installKiwiTCMS:
    node.bindRole(RoleBinding("kiwitcms"))
if params.installDagster:
    node.bindRole(RoleBinding("dagster"))
node.addService(pg.Execute(shell="sh",command=HEAD_CMD))
k = 0
for x in params.sharedVlans:
    iface = node.addInterface("ifSharedVlan%d" % (k,))
    if x.sharedVlanAddress:
        iface.addAddress(
            pg.IPv4Address(x.sharedVlanAddress,x.sharedVlanNetmask))
    sharedvlan = pg.Link('shared-vlan-%d' % (k,))
    sharedvlan.addInterface(iface)
    if x.createConnectableSharedVlan:
        sharedvlan.enableSharedVlan()
    else:
        if x.createSharedVlan:
            sharedvlan.createSharedVlan(x.sharedVlanName)
        else:
            sharedvlan.connectSharedVlan(x.sharedVlanName)
    sharedvlan.link_multiplexing = True
    sharedvlan.best_effort = True
    sharedvlans.append(sharedvlan)
    k += 1
#sharedvlan._ext_children.append(Label("role","inter-expt",kind="ansible"))

request.addResource(node)
for x in sharedvlans:
    request.addResource(x)

request.addResource(EmulabEncrypt("perExptPassword"))

pc.printRequestRSpec(request)
